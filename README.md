# SmileInstagramBundle

## Installation


Register the bundle in `app/AppKernel.php`:

```php
// app/AppKernel.php
public function registerBundles()
{
    return [
        // ...
        new Smile\InstagramBundle\SmileInstagramBundle(),
    ];
}
```

Add the following to `app/config/config.yml`:

```yaml
imports:
    - { resource: "@SmileInstagramBundle/Resources/config/services.yml" }

smile_instagram:
    instaphp:
        config:
            client_id: %instagram_api_client_id%
            client_secret: %instagram_api_client_secret%
```

And add these parameters:

```yaml
instagram_api_client_id: YOUR_API_ID
instagram_api_client_secret: YOUR_API_SECRET
```

And if you're OK with the provided routes, add these to
`app/config/routing.yml`:

```yaml
SmileInstagramBundle:
    resource: "@SmileInstagramBundle/Resources/config/routing.yml"
    prefix:   /
```

## Usage (Controller)

```php
/**
 * @var InstaphpAdapter
 */
$api = $this->get('instagram_php');

$response = $api->Media->Comments($query);
```

You can also test if a user is logged in:

```php
//is a user logged in?
$loggedIn = $this->get('instaphp_token_handler')->isLoggedIn();
```

And you also get url for OAuth:
```php
$this->get("instagram_api")->getOauthUrl()
```

## Instagram Auth Token
 
### UserToken
 
The Instagram auth code is stored in the User Entity. The methods
`setInstagramAuthCode()` and `getInstagramAuthCode()` must be implemented on
your User.

```yaml
services:
    instaphp_token_handler:
        class: Smile\InstagramBundle\TokenHandler\UserToken
            arguments: ["@security.context", "@doctrine.orm.default_entity_manager"]
```
