<?php

namespace Smile\InstagramBundle\Controller;

use Instaphp\Exceptions\Exception;
use Smile\InstagramBundle\Adapter\InstaphpAdapter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class InstagramController extends Controller
{

	/**
	 * Callback для OAuth
	 *
	 * @param Request $request
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse
	 */
	public function callbackAction(Request $request)
	{

		$code = $request->get('code');

		if (!empty($code))
		{
			$api = $this->get('instagram_api');

			try {
				//-- Authenticate
				$success = $api->Users->Authorize($code);

				$token = $api->getAccessToken();

				$isLoggedIn = $this->get('instaphp_token_handler')->setToken($token);

				$this->get('session')->getFlashBag()->add('loggedin', 'Thanks for logging in');

				return $this->redirect($this->generateUrl($this->container->getParameter('instaphp.redirect_route_login')));
			}
			catch(Exception $e)
			{
				throw $this->createNotFoundException('Invalid Request', $e);
			}
			catch (\Exception $e)
			{
				throw $this->createNotFoundException('Error', $e);
			}
		}

		throw $this->createNotFoundException('Invalid Request');
	}

	public function testAction(Request $request){
	    dump($request);
    }
}
