<?php

namespace Smile\InstagramBundle\Instagram;


use Smile\InstagramBundle\Entity\Comment;

class CommentResponse extends Response
{

    /**
     * @return array|Comment[]
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param CommentResponse $data
     */
    function setData($data)
    {
        $this->data = $data;

        return $this;
    }
}