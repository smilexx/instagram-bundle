<?php
/**
 * Created by PhpStorm.
 * User: smile
 * Date: 21.01.2018
 * Time: 17:56
 */

namespace Smile\InstagramBundle\Instagram;


abstract class Response
{
    /**
     * @var array
     */
    protected $meta;

    /**
     * @var array
     */
    protected $data;

    /**
     * @return array
     */
    abstract public function getData();

    /**
     * @param array $data
     */
    abstract function setData($data);

    /**
     * @return array
     */
    public function getMeta()
    {
        return $this->meta;
    }

    /**
     * @param array $meta
     */
    public function setMeta($meta)
    {
        $this->meta = $meta;
    }


}