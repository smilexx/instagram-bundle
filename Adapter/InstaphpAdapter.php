<?php

namespace Smile\InstagramBundle\Adapter;

use Instaphp\Instaphp;
use Instaphp\Exceptions\InstaphpException;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Monolog\Logger;

/**
 * Class InstaphpAdapter
 * @package Smile\InstagramBundle\Adapter
 */
class InstaphpAdapter extends Instaphp
{
    /**
     * @var array Доступные точки входа
     */
    protected static $availableEndpoints = ['media', 'users'];

    public function __construct($tokenClass = null, $config = array(), RouterInterface $router = null)
    {
        $ua = sprintf('Instaphp/2.0; cURL/%s; (+http://instaphp.com)', curl_version()['version']);

        $defaults = [
            'client_id' => '',
            'client_secret' => '',
            'access_token' => '',
            'redirect_uri' => '',
            'client_ip' => '',
            'scope' => 'comments',
            'log_enabled' => true,
            'log_level' => Logger::DEBUG,
            'api_protocol' => 'https',
            'api_host' => 'api.instagram.com',
            'api_version' => 'v1',
            'http_useragent' => $ua,
            'http_timeout' => 6,
            'http_connect_timeout' => 2,
            'verify' => true,
            'debug' => false,
            'event.before' => [],
            'event.after' => [],
            'event.error' => [],
            'oauth_token_path' => 'oauth/access_token',
            'redirect_route' => 'SmileInstagramBundle_callback',
        ];

        if ($router) {
            $config['redirect_uri'] = $router->generate(
                $config['redirect_route'],
                array(),
                UrlGeneratorInterface::ABSOLUTE_URL
            );
        }

        $this->config = $config + $defaults;

        // Проверка на client_id
        if (empty($this->config['client_id'])) {
            throw new InstaphpException('Invalid client id');
        }

        // Получение токена
        if (empty($this->config['access_token'])) {
            if ($tokenClass instanceof TokenHandlerInterface) {
                $token = $tokenClass->getToken();
            } else {
                $token = null;
            }

            $this->setAccessToken($token);
        }
    }

    public function getComments($mediaId)
    {
        return $this->Media->Comments($mediaId);

    }
}
